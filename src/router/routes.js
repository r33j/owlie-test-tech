
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    name: 'landing'
  },
  {
    path: '/login',
    component: () => import('layouts/Login'),
    name: 'login'
  },
  {
    path: '/register',
    component: () => import('layouts/Register'),
    name: 'register'
  },
  {
    path: '/dashboard',
    component: () => import('layouts/Dashboard'),
    name: 'dashboard',
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/dashboard/profile',
    component: () => import('layouts/Profile'),
    name: 'profile',
    meta: {
      requiresAuth: true
    }
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
