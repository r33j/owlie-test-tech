
# Quasar App (owlie-test-tech)

*Owlie test technique - Roman MERCK - Sept 2020*

# I. How to: Install / Run / Build project



# II. Découpe du projet



# III. Problèmes rencontrés

# IV. Si soucis au lancement

......................................................................

  # I. How to: Install / Run / Build project

## if Quasar is not already globally install do:
```
npm install -g @quasar/cli
```
## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


# II. Découpe du projet

Le projet utilise comme souhaité Quasar (créé via CLI en global).
Sont utilisés: **router**, **vuex**, et **firebase** (boot généré). Styling en .scss

Découpe Composant / Layout. La barre de navigation (composant **toolbar**), prend un composant  **menu**  (le menu déroulant de gauche).

Utilisation de **firebase : realtime database** pour les notes.

Authentification gérée via Firebase auth. Lors du login : un state dans la store (vuex), prend un bool *true*, de fait tous les composants ne devant apparaître aux utilisateurs non identifiés en dépendent.

# III. Problèmes rencontrés

Non connaissance préalable de Quasar.
Une prise main relativement rapide, toutefois les tweaks avenant à la framework qui m'auront pris du temps sont:

 - pas de fichier main.js, gestion via fichiers de conf etc..
 - gestion du router différente
 - gestion de vuex différente

Ainsi ces petits soucis m'auront couté du temps sur des ajouts de fonctionnalités comme: ajouter du texte dans les notes / éditer celles-ci. Pouvoir les supprimer.

De surcroit je n'ai percuté qu'aux 3/4 du développement qu'il me fallait utiliser Firestore et non la realtime database, c'était, du coup, trop délicat pour switch et revenir sur ce l'existant en restant dans les temps.
# IV. Si soucis au lancement.

Bien vérifier dans quasar.conf.js la présence de :

    boot: [
      'firebase',<<<<<
      'axios'
    ],

Ainsi que de l'existence en racine du fichier de conf firebase:
```
 firebase.conf.js
```

ps: un warning au render peut parfois nécessiter d'appuyer deux fois sur le bouton au log.
Toutefois avec le manque de temps, j'ai laissé le warn de coté. Merci.
